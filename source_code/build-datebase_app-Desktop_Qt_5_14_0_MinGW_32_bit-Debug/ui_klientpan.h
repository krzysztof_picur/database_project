/********************************************************************************
** Form generated from reading UI file 'klientpan.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KLIENTPAN_H
#define UI_KLIENTPAN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_klientpan
{
public:
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QCalendarWidget *calendarWidget;

    void setupUi(QDialog *klientpan)
    {
        if (klientpan->objectName().isEmpty())
            klientpan->setObjectName(QString::fromUtf8("klientpan"));
        klientpan->resize(723, 500);
        klientpan->setStyleSheet(QString::fromUtf8("#klientpan{\n"
"background-image:url(:/items/bacground_client.jpg);\n"
"}\n"
""));
        groupBox = new QGroupBox(klientpan);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(70, 340, 571, 81));
        groupBox->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"background-color: rgb(233, 185, 110);\n"
"\n"
"}"));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(groupBox);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(groupBox);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout->addWidget(pushButton_3);

        calendarWidget = new QCalendarWidget(klientpan);
        calendarWidget->setObjectName(QString::fromUtf8("calendarWidget"));
        calendarWidget->setEnabled(false);
        calendarWidget->setGeometry(QRect(170, 80, 344, 211));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        calendarWidget->setFont(font);
        calendarWidget->setMouseTracking(false);
        calendarWidget->setGridVisible(false);
        calendarWidget->setSelectionMode(QCalendarWidget::NoSelection);
        calendarWidget->setDateEditEnabled(true);

        retranslateUi(klientpan);

        QMetaObject::connectSlotsByName(klientpan);
    } // setupUi

    void retranslateUi(QDialog *klientpan)
    {
        klientpan->setWindowTitle(QCoreApplication::translate("klientpan", "Panel u\305\274ytkownika", nullptr));
        groupBox->setTitle(QString());
#if QT_CONFIG(tooltip)
        pushButton->setToolTip(QCoreApplication::translate("klientpan", "<html><head/><body><p><span style=\" font-weight:600;\">Przegl\304\205daj w\305\202asne wypo\305\274yczenia, lub przed\305\202u\305\274 wypo\305\274yczenie</span></p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        pushButton->setText(QCoreApplication::translate("klientpan", "Moje wypo\305\274yczenia", nullptr));
        pushButton_2->setText(QCoreApplication::translate("klientpan", "Wyszukaj Ksi\304\205\305\274k\304\231", nullptr));
        pushButton_3->setText(QCoreApplication::translate("klientpan", "Powr\303\263t do logowania", nullptr));
    } // retranslateUi

};

namespace Ui {
    class klientpan: public Ui_klientpan {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KLIENTPAN_H
