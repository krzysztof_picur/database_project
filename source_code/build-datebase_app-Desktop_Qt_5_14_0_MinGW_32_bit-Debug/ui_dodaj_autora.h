/********************************************************************************
** Form generated from reading UI file 'dodaj_autora.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DODAJ_AUTORA_H
#define UI_DODAJ_AUTORA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_dodaj_autora
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_8;
    QLineEdit *imie;
    QLabel *label_9;
    QLineEdit *nazwisko;
    QLabel *label_10;
    QLineEdit *drugie_imie;
    QLabel *label_11;
    QLineEdit *pseudonim;
    QLabel *label_12;
    QLineEdit *email;
    QLabel *label_13;
    QComboBox *noblista;
    QPushButton *dodaj_autora_2;

    void setupUi(QDialog *dodaj_autora)
    {
        if (dodaj_autora->objectName().isEmpty())
            dodaj_autora->setObjectName(QString::fromUtf8("dodaj_autora"));
        dodaj_autora->resize(293, 456);
        dodaj_autora->setStyleSheet(QString::fromUtf8("#dodaj_autora{\n"
"background-color: rgb(28, 255, 187);}\n"
""));
        verticalLayout = new QVBoxLayout(dodaj_autora);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        widget_2 = new QWidget(dodaj_autora);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"background-color: rgb(233, 185, 110);\n"
"border-radius: 10px;\n"
"}"));
        verticalLayout_3 = new QVBoxLayout(widget_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_8 = new QLabel(widget_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        verticalLayout_3->addWidget(label_8);

        imie = new QLineEdit(widget_2);
        imie->setObjectName(QString::fromUtf8("imie"));
        imie->setClearButtonEnabled(true);

        verticalLayout_3->addWidget(imie);

        label_9 = new QLabel(widget_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        verticalLayout_3->addWidget(label_9);

        nazwisko = new QLineEdit(widget_2);
        nazwisko->setObjectName(QString::fromUtf8("nazwisko"));
        nazwisko->setClearButtonEnabled(true);

        verticalLayout_3->addWidget(nazwisko);

        label_10 = new QLabel(widget_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        verticalLayout_3->addWidget(label_10);

        drugie_imie = new QLineEdit(widget_2);
        drugie_imie->setObjectName(QString::fromUtf8("drugie_imie"));
        drugie_imie->setClearButtonEnabled(true);

        verticalLayout_3->addWidget(drugie_imie);

        label_11 = new QLabel(widget_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        verticalLayout_3->addWidget(label_11);

        pseudonim = new QLineEdit(widget_2);
        pseudonim->setObjectName(QString::fromUtf8("pseudonim"));

        verticalLayout_3->addWidget(pseudonim);

        label_12 = new QLabel(widget_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        verticalLayout_3->addWidget(label_12);

        email = new QLineEdit(widget_2);
        email->setObjectName(QString::fromUtf8("email"));
        email->setClearButtonEnabled(true);

        verticalLayout_3->addWidget(email);

        label_13 = new QLabel(widget_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        verticalLayout_3->addWidget(label_13);

        noblista = new QComboBox(widget_2);
        noblista->addItem(QString());
        noblista->addItem(QString());
        noblista->setObjectName(QString::fromUtf8("noblista"));

        verticalLayout_3->addWidget(noblista);


        verticalLayout_2->addWidget(widget_2);


        verticalLayout->addLayout(verticalLayout_2);

        dodaj_autora_2 = new QPushButton(dodaj_autora);
        dodaj_autora_2->setObjectName(QString::fromUtf8("dodaj_autora_2"));
        dodaj_autora_2->setStyleSheet(QString::fromUtf8("background-color: rgb(16, 255, 32);"));

        verticalLayout->addWidget(dodaj_autora_2);


        retranslateUi(dodaj_autora);

        QMetaObject::connectSlotsByName(dodaj_autora);
    } // setupUi

    void retranslateUi(QDialog *dodaj_autora)
    {
        dodaj_autora->setWindowTitle(QCoreApplication::translate("dodaj_autora", "Dodaj autora", nullptr));
        label_8->setText(QCoreApplication::translate("dodaj_autora", "Imi\304\231", nullptr));
        label_9->setText(QCoreApplication::translate("dodaj_autora", "Nazwisko", nullptr));
        label_10->setText(QCoreApplication::translate("dodaj_autora", "Drugie imi\304\231", nullptr));
        label_11->setText(QCoreApplication::translate("dodaj_autora", "Pseudonim", nullptr));
        label_12->setText(QCoreApplication::translate("dodaj_autora", "Email", nullptr));
        label_13->setText(QCoreApplication::translate("dodaj_autora", "Noblista", nullptr));
        noblista->setItemText(0, QCoreApplication::translate("dodaj_autora", "NIE", nullptr));
        noblista->setItemText(1, QCoreApplication::translate("dodaj_autora", "TAk", nullptr));

        dodaj_autora_2->setText(QCoreApplication::translate("dodaj_autora", "Dodaj", nullptr));
    } // retranslateUi

};

namespace Ui {
    class dodaj_autora: public Ui_dodaj_autora {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DODAJ_AUTORA_H
