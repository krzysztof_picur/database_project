#ifndef WYSWIETL_UZYTKOWNIKOW_H
#define WYSWIETL_UZYTKOWNIKOW_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>

namespace Ui {
class wyswietl_uzytkownikow;
}

class wyswietl_uzytkownikow : public QDialog
{
    Q_OBJECT

public:
    explicit wyswietl_uzytkownikow(QWidget *parent = 0);
    ~wyswietl_uzytkownikow();

private:
    Ui::wyswietl_uzytkownikow *ui;
    QSqlQueryModel *querymodel;
};

#endif // WYSWIETL_UZYTKOWNIKOW_H
