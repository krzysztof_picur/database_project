#include "dodaj_ksiazke.h"
#include "ui_dodaj_ksiazke.h"

dodaj_ksiazke::dodaj_ksiazke(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dodaj_ksiazke)
{
    ui->setupUi(this);
}

dodaj_ksiazke::~dodaj_ksiazke()
{
    delete ui;
}

void dodaj_ksiazke::on_pushButton_clicked()
{
    QString tytul=ui->tytul->text();
    QString podtytul=ui->podtytul->text();
    QString gatunek=ui->gatunek->currentText();
    QString oprawa=ui->oprawa->currentText();
    QString ISBN=ui->ISBN->text();
    QString naklad=ui->naklad->text();

    QSqlQuery qry1;

    qry1.prepare("INSERT INTO ksiazka(tytul, podtytul, gatunek, oprawa, ISBN, naklad)"
                "VALUES (:tytul, :podtytul, :gatunek, :oprawa, :ISBN, :naklad)");

    qry1.bindValue(":tytul", tytul);
    qry1.bindValue(":podtyul", podtytul);
    qry1.bindValue(":gatunek", gatunek);
    qry1.bindValue(":oprawa", oprawa);
    qry1.bindValue(":ISBN", ISBN);
    qry1.bindValue(":naklad", naklad);


    if(qry1.exec()){
        QMessageBox::information(this, "Dodaj książkę", "Książka została dodana");
    }else{
        QMessageBox::warning(this, "Dodaj ksiażkę", "Wystąpił błąd - książka nie została dodana");
    }

    this->hide();

    QWidget *parent = this->parentWidget();
    parent->show();

    //Czyszczenie wszystkich QlineEdit w formularzu
   foreach(QLineEdit* le, findChildren<QLineEdit*>()) {
    le->clear();
    }
}
