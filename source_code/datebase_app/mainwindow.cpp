#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    hide();
    usersig=new usersign(this);
    usersig->show();
}


void MainWindow::on_pushButton_2_clicked()
{
    hide();
    adminsig=new adminsign(this);
    adminsig->show();
}


void MainWindow::on_pushButton_3_clicked()
{
    QSqlDatabase db=QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("127.0.0.1");
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("biblioteka");

    if(db.open()){
        QMessageBox::information(this, "Connection", "Udało się połączyć z bazą danych");
    }else{
        QMessageBox::warning(this, "Connection", "Nie udało się połączyć z bazą danych");
    }
}
