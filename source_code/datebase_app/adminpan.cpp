#include "adminpan.h"
#include "ui_adminpan.h"
#include "zasoby_biblioteki.h"

adminpan::adminpan(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::adminpan)
{
    ui->setupUi(this);
}

adminpan::~adminpan()
{
    delete ui;
}

void adminpan::on_dodaj_ksiazke_clicked()
{
    dodajksiazke=new dodaj_ksiazke(this);
    dodajksiazke->show();
}

void adminpan::on_dodaj_autora_clicked()
{
    dodajautora=new dodaj_autora(this);
    dodajautora->show();
}

void adminpan::on_przydziel_autora_clicked()
{
    przydzielautora=new przydziel_autora(this);
    przydzielautora->show();
}

void adminpan::on_zasoby_biblioteki_clicked()
{
    zasobybiblioteki=new zasoby_biblioteki(this);
    zasobybiblioteki->show();

}

void adminpan::on_dodaj_wydawnictwo_clicked()
{
    dodajwydawnictwo=new dodaj_wydawnictwo(this);
    dodajwydawnictwo->show();
}

void adminpan::on_pushButton_clicked()
{
    this->close();

    QWidget *parent = this->parentWidget();
    parent->show();

}

void adminpan::on_uzytkownicy_systemu_clicked()
{
    wyswietluzytkownikow=new wyswietl_uzytkownikow(this);
    wyswietluzytkownikow->show();
}

void adminpan::on_znajdz_ksiazke_clicked()
{
    znajdzksiazke=new znajdz_ksiazke(this);
    znajdzksiazke->show();
}

void adminpan::on_zarzadzaj_wypozyczeniami_clicked()
{
    zarzadzajwypozyczeniami=new zarzadzaj_wypozyczeniami(this);
    zarzadzajwypozyczeniami->show();
}

void adminpan::on_zarzadzaj_uzytkownikami_clicked()
{
    zarzadzajuzytkownikami=new zarzadzaj_uzytkownikami(this);
    zarzadzajuzytkownikami->show();
}
