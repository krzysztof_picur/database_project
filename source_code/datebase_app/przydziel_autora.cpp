#include "przydziel_autora.h"
#include "ui_przydziel_autora.h"

przydziel_autora::przydziel_autora(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::przydziel_autora)
{
    ui->setupUi(this);
}

przydziel_autora::~przydziel_autora()
{
    delete ui;
}

void przydziel_autora::on_pushButton_3_clicked()
{
    QString imie_2=ui->imie_2->currentText();
    QString nazwisko_2=ui->nazwisko_2->currentText();
    QString tytul_2=ui->tytul_2->currentText();

    QSqlQuery qry3;

    qry3.prepare("INSERT INTO autor_ksiazka(id_autora, id_ksiazki)"
            "VALUES ((SELECT id_autora FROM autor WHERE imie LIKE :imie_2 AND nazwisko LIKE :nazwisko_2), (SELECT id_ksiazki from ksiazka WHERE tytul LIKE :tytul_2))");

    qry3.bindValue(":imie_2", imie_2);
    qry3.bindValue(":nazwisko_2", nazwisko_2);
    qry3.bindValue(":tytul_2", tytul_2);

    if(qry3.exec()){
        QMessageBox::information(this, "Dodaj Autora do książki", "Autor został dodany");
    }else{
        QMessageBox::warning(this, "Dodaj autora do ksiazki", "Wystąpił błąd - autor nie został dodany");
    }

    QWidget *parent = this->parentWidget();
    parent->show();

}

void przydziel_autora::on_pobierz_imie_clicked()
{
    QSqlQuery query;
        query.prepare("SELECT imie FROM autor");

        query.exec();

        while (query.next()){
            int imieAutora=query.record().indexOf("imie");
            QString a = query.value(imieAutora).toString();
            ui->imie_2->addItem(a);
    }
}

void przydziel_autora::on_pobierz_nazwisko_clicked()
{
    QSqlQuery query;
        query.prepare("SELECT nazwisko FROM autor");

        query.exec();

        while (query.next()){
            int nazwiskoAutora=query.record().indexOf("nazwisko");
            QString a = query.value(nazwiskoAutora).toString();
            ui->nazwisko_2->addItem(a);
    }
}

void przydziel_autora::on_pobierz_tytul_clicked()
{
    QSqlQuery query;
        query.prepare("SELECT tytul FROM ksiazka");

        query.exec();

        while (query.next()){
            int tytulKsiazki=query.record().indexOf("tytul");
            QString a = query.value(tytulKsiazki).toString();
            ui->tytul_2->addItem(a);
    }
}
