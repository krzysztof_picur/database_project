#include "dodaj_wydawnictwo.h"
#include "ui_dodaj_wydawnictwo.h"

dodaj_wydawnictwo::dodaj_wydawnictwo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dodaj_wydawnictwo)
{
    ui->setupUi(this);
}

dodaj_wydawnictwo::~dodaj_wydawnictwo()
{
    delete ui;
}

void dodaj_wydawnictwo::on_Dodaj_clicked()
{
    QString nazwa=ui->nazwa->text();
    QString data_wydania=ui->data_wydania->text();
    QString miejsce_wydania=ui->miejsce_wydania->text();
    QString typ_publikacji=ui->typ_publikacji->currentText();
    QString liczba_stron_wydania=ui->liczba_stron_wydania->text();
    QString id_ksiazki=ui->id_ksiazki->currentText();

    QSqlQuery qry4;

    qry4.prepare("INSERT INTO wydawnictwo(nazwa, data_wydania, miejsce_wydania, typ_publikacji, liczba_stron_wydania, id_ksiazki)"
                "VALUES (:nazwa, :data_wydania, :miejsce_wydania, :typ_publikacji, :liczba_stron_wydania, :id_ksiazki)");

    qry4.bindValue(":nazwa", nazwa);
    qry4.bindValue(":data_wydania", data_wydania);
    qry4.bindValue(":miejsce_wydania", miejsce_wydania);
    qry4.bindValue(":typ_publikacji", typ_publikacji);
    qry4.bindValue(":liczba_stron_wydania", liczba_stron_wydania);
    qry4.bindValue(":id_ksiazki", id_ksiazki);


    if(qry4.exec()){
        QMessageBox::information(this, "Dodaj wydawnictwo", "Wydawnictwo zostało dodane");
    }else{
        QMessageBox::warning(this, "Dodaj wydawnictwo", "Wystąpił błąd - wydawnictwo nie zostało dodane");
    }

    this->hide();

    QWidget *parent = this->parentWidget();
    parent->show();

    //Czyszczenie wszystkich QlineEdit w formularzu
   foreach(QLineEdit* le, findChildren<QLineEdit*>()) {
    le->clear();
    }
}

void dodaj_wydawnictwo::on_pobierz_liste_clicked()
{
    QSqlQuery query;
        query.prepare("SELECT id_ksiazki, tytul FROM Ksiazka");

        query.exec();

        while (query.next()){
            int ksiazkaID=query.record().indexOf("id_ksiazki");
            int ksiazkaTytul=query.record().indexOf("tytul");
            QString a = query.value(ksiazkaID).toString();
            QString b = query.value(ksiazkaTytul).toString();
            QString n = a+" "+"("+b+")";
            ui->id_ksiazki->addItem(n);
    }

}
