#ifndef ZNAJDZ_KSIAZKE_H
#define ZNAJDZ_KSIAZKE_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>

namespace Ui {
class znajdz_ksiazke;
}

class znajdz_ksiazke : public QDialog
{
    Q_OBJECT

public:
    explicit znajdz_ksiazke(QWidget *parent = 0);
    ~znajdz_ksiazke();

private slots:
    void on_Szukaj_ksiazki_clicked();

private:
    Ui::znajdz_ksiazke *ui;

};

#endif // ZNAJDZ_KSIAZKE_H
