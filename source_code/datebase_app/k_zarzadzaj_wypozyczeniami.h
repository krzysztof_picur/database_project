#ifndef K_ZARZADZAJ_WYPOZYCZENIAMI_H
#define K_ZARZADZAJ_WYPOZYCZENIAMI_H

#include <QDialog>
#include <QSqlDatabase>
#include <QtSql>
#include "klientpan.h"

namespace Ui {
class k_zarzadzaj_wypozyczeniami;
}

class k_zarzadzaj_wypozyczeniami : public QDialog
{
    Q_OBJECT

public:
    explicit k_zarzadzaj_wypozyczeniami(QWidget *parent = nullptr);
    ~k_zarzadzaj_wypozyczeniami();
    void klientid(int);

private slots:


    void on_pushButton_clicked();

private:
    Ui::k_zarzadzaj_wypozyczeniami *ui;
    QSqlRelationalTableModel *tableModel;
    QSqlRelationalDelegate *delegate;
};

#endif // K_ZARZADZAJ_WYPOZYCZENIAMI_H
