#include "adminsign.h"
#include "ui_adminsign.h"
#include <QSqlDatabase>
#include <QtSql>


adminsign::adminsign(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::adminsign)
{
    ui->setupUi(this);
}

adminsign::~adminsign()
{
    delete ui;
}

/********************Logowanie admina****************************************/
void adminsign::on_adminlog_clicked()
{
    QString username, password;

    username=ui->adminlogin->text();
    password=ui->adminpassword->text();

    QSqlQuery query;

    if(query.exec("select * from dane_admina where nazwa_admina='"+username+"' and haslo_admina='"+password+"'")){
        int count=0;
        while(query.next()){
            count ++;
        }
        if(count==1){
             QMessageBox::information(this, "Logowanie", "Logowanie powiodło się");

            adminpanel=new adminpan(this);
            adminpanel->show();
             hide();
        }
        if(count <1){
             QMessageBox::warning(this, "Logowanie", "Logowanie się nie powiodło");
        }
    }
    //Czyszczenie wszystkich QlineEdit w formularzu
    foreach(QLineEdit* le, findChildren<QLineEdit*>()) {
    le->clear();
    }
}
/**************************************************************************************************/

void adminsign::on_pushButton_clicked()
{
    this->close();

    QWidget *parent = this->parentWidget();
    parent->show();

}
