#ifndef DODAJ_AUTORA_H
#define DODAJ_AUTORA_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>

namespace Ui {
class dodaj_autora;
}

class dodaj_autora : public QDialog
{
    Q_OBJECT

public:
    explicit dodaj_autora(QWidget *parent = nullptr);
    ~dodaj_autora();

private slots:
    void on_dodaj_autora_2_clicked();

private:
    Ui::dodaj_autora *ui;
};

#endif // DODAJ_AUTORA_H
