#ifndef ZARZADZAJ_WYPOZYCZENIAMI_H
#define ZARZADZAJ_WYPOZYCZENIAMI_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>


namespace Ui {
class zarzadzaj_wypozyczeniami;
}

class zarzadzaj_wypozyczeniami : public QDialog
{
    Q_OBJECT

public:
    explicit zarzadzaj_wypozyczeniami(QWidget *parent = 0);
    ~zarzadzaj_wypozyczeniami();

private:
    Ui::zarzadzaj_wypozyczeniami *ui;
    QSqlRelationalTableModel *tableModel;
    QSqlRelationalDelegate *delegate;
};

#endif // ZARZADZAJ_WYPOZYCZENIAMI_H
