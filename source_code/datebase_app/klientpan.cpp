#include "klientpan.h"
#include "ui_klientpan.h"


klientpan::klientpan(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::klientpan)
{
    ui->setupUi(this);


}

void klientpan::on_calendarWidget_customContextMenuRequested(const QPoint &pos)
{

}

void klientpan::on_pushButton_2_clicked()
{
    k_znajdz_ksiazke *kznajdz_ksiazke;
    hide();
    kznajdz_ksiazke=new k_znajdz_ksiazke(this);
    kznajdz_ksiazke->show();
}

void klientpan::on_pushButton_clicked()
{
    k_zarzadzaj_wypozyczeniami *kzarzadzaj_wypozyczeniami;
    hide();
    kzarzadzaj_wypozyczeniami = new k_zarzadzaj_wypozyczeniami(this);
    kzarzadzaj_wypozyczeniami->show();
}

klientpan::~klientpan()
{
    delete ui;
}

void klientpan::on_pushButton_3_clicked()
{
    this->close();

    QWidget *parent = this->parentWidget();
    parent->show();
}
