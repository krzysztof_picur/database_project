#ifndef ZNAJDZ_KSIAZKE_WYNIK_H
#define ZNAJDZ_KSIAZKE_WYNIK_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql>

namespace Ui {
class znajdz_ksiazke_wynik;
}

class znajdz_ksiazke_wynik : public QDialog
{
    Q_OBJECT

public:
    explicit znajdz_ksiazke_wynik(QWidget *parent = 0);
    ~znajdz_ksiazke_wynik();

private:
    Ui::znajdz_ksiazke_wynik *ui;
    QSqlQueryModel *querymodel;
};

#endif // ZNAJDZ_KSIAZKE_WYNIK_H
