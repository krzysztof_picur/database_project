#ifndef KLIENTPAN_H
#define KLIENTPAN_H

#include <QDialog>
#include <QCalendarWidget>
#include "k_znajdz_ksiazke.h"
#include "k_zarzadzaj_wypozyczeniami.h"

namespace Ui {
class klientpan;
}

class klientpan : public QDialog
{
    Q_OBJECT

public:
    explicit klientpan(QWidget *parent = 0);
    ~klientpan();

private slots:


    void on_calendarWidget_customContextMenuRequested(const QPoint &pos);

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::klientpan *ui;

};

#endif // KLIENTPAN_H
