#ifndef ADMINSIGN_H
#define ADMINSIGN_H

#include <QDialog>
#include <QMessageBox>
#include "adminpan.h"

namespace Ui {
class adminsign;
}

class adminsign : public QDialog
{
    Q_OBJECT

public:
    explicit adminsign(QWidget *parent = 0);
    ~adminsign();

private slots:
    void on_adminlog_clicked();

    void on_pushButton_clicked();

private:
    Ui::adminsign *ui;
    adminpan *adminpanel;
};

#endif // ADMINSIGN_H
