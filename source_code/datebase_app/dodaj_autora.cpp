#include "dodaj_autora.h"
#include "ui_dodaj_autora.h"

dodaj_autora::dodaj_autora(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dodaj_autora)
{
    ui->setupUi(this);
}

dodaj_autora::~dodaj_autora()
{
    delete ui;
}

void dodaj_autora::on_dodaj_autora_2_clicked()
{
    QString imie=ui->imie->text();
    QString nazwisko=ui->nazwisko->text();
    QString drugie_imie=ui->drugie_imie->text();
    QString pseudonim=ui->pseudonim->text();
    QString email=ui->email->text();
    QString noblista=ui->noblista->currentText();

    QSqlQuery qry2;

    qry2.prepare("INSERT INTO autor(imie, nazwisko, drugie_imie, pseudonim, email, noblista)"
                "VALUES (:imie, :nazwisko, :drugie_imie, :pseudonim, :email, :noblista)");

    qry2.bindValue(":imie", imie);
    qry2.bindValue(":nazwisko", nazwisko);
    qry2.bindValue(":drugie_imie", drugie_imie);
    qry2.bindValue(":pseudonim", pseudonim);
    qry2.bindValue(":email", email);
    qry2.bindValue(":noblista", noblista);

    if(qry2.exec()){
        QMessageBox::information(this, "Dodaj autora", "Autor został dodany");
    }else{
        QMessageBox::warning(this, "Dodaj autora", "Wystąpił błąd - autor nie został dodany");
    }
    this->hide();

    QWidget *parent = this->parentWidget();
    parent->show();

    //Czyszczenie wszystkich QlineEdit w formularzu
   foreach(QLineEdit* le, findChildren<QLineEdit*>()) {
    le->clear();
}
}

