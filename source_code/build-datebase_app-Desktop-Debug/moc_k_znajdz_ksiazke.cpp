/****************************************************************************
** Meta object code from reading C++ file 'k_znajdz_ksiazke.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../database_project/datebase_app/k_znajdz_ksiazke.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'k_znajdz_ksiazke.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_k_znajdz_ksiazke_t {
    QByteArrayData data[9];
    char stringdata0[139];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_k_znajdz_ksiazke_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_k_znajdz_ksiazke_t qt_meta_stringdata_k_znajdz_ksiazke = {
    {
QT_MOC_LITERAL(0, 0, 16), // "k_znajdz_ksiazke"
QT_MOC_LITERAL(1, 17, 22), // "on_tableView_activated"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 11), // "QModelIndex"
QT_MOC_LITERAL(4, 53, 5), // "index"
QT_MOC_LITERAL(5, 59, 17), // "on_szukaj_clicked"
QT_MOC_LITERAL(6, 77, 19), // "on_wypozycz_clicked"
QT_MOC_LITERAL(7, 97, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(8, 119, 19) // "on_szukaj_2_clicked"

    },
    "k_znajdz_ksiazke\0on_tableView_activated\0"
    "\0QModelIndex\0index\0on_szukaj_clicked\0"
    "on_wypozycz_clicked\0on_pushButton_clicked\0"
    "on_szukaj_2_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_k_znajdz_ksiazke[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x08 /* Private */,
       5,    0,   42,    2, 0x08 /* Private */,
       6,    0,   43,    2, 0x08 /* Private */,
       7,    0,   44,    2, 0x08 /* Private */,
       8,    0,   45,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void k_znajdz_ksiazke::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        k_znajdz_ksiazke *_t = static_cast<k_znajdz_ksiazke *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_tableView_activated((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 1: _t->on_szukaj_clicked(); break;
        case 2: _t->on_wypozycz_clicked(); break;
        case 3: _t->on_pushButton_clicked(); break;
        case 4: _t->on_szukaj_2_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject k_znajdz_ksiazke::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_k_znajdz_ksiazke.data,
      qt_meta_data_k_znajdz_ksiazke,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *k_znajdz_ksiazke::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *k_znajdz_ksiazke::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_k_znajdz_ksiazke.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int k_znajdz_ksiazke::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
