/********************************************************************************
** Form generated from reading UI file 'k_zarzadzaj_wypozyczeniami.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_K_ZARZADZAJ_WYPOZYCZENIAMI_H
#define UI_K_ZARZADZAJ_WYPOZYCZENIAMI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>

QT_BEGIN_NAMESPACE

class Ui_k_zarzadzaj_wypozyczeniami
{
public:
    QPushButton *pushButton;
    QTableView *tableView;

    void setupUi(QDialog *k_zarzadzaj_wypozyczeniami)
    {
        if (k_zarzadzaj_wypozyczeniami->objectName().isEmpty())
            k_zarzadzaj_wypozyczeniami->setObjectName(QStringLiteral("k_zarzadzaj_wypozyczeniami"));
        k_zarzadzaj_wypozyczeniami->resize(951, 638);
        k_zarzadzaj_wypozyczeniami->setStyleSheet(QLatin1String("#k_zarzadzaj_wypozyczeniami{\n"
"background-image: url(:/items/bacground_client.jpg);\n"
"}"));
        pushButton = new QPushButton(k_zarzadzaj_wypozyczeniami);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(770, 570, 131, 23));
        tableView = new QTableView(k_zarzadzaj_wypozyczeniami);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setGeometry(QRect(20, 40, 911, 491));

        retranslateUi(k_zarzadzaj_wypozyczeniami);

        QMetaObject::connectSlotsByName(k_zarzadzaj_wypozyczeniami);
    } // setupUi

    void retranslateUi(QDialog *k_zarzadzaj_wypozyczeniami)
    {
        k_zarzadzaj_wypozyczeniami->setWindowTitle(QApplication::translate("k_zarzadzaj_wypozyczeniami", "Wypo\305\274yczenia", nullptr));
        pushButton->setText(QApplication::translate("k_zarzadzaj_wypozyczeniami", "Panel Klienta", nullptr));
    } // retranslateUi

};

namespace Ui {
    class k_zarzadzaj_wypozyczeniami: public Ui_k_zarzadzaj_wypozyczeniami {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_K_ZARZADZAJ_WYPOZYCZENIAMI_H
