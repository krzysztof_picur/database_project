/********************************************************************************
** Form generated from reading UI file 'zasoby_biblioteki.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZASOBY_BIBLIOTEKI_H
#define UI_ZASOBY_BIBLIOTEKI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_zasoby_biblioteki
{
public:
    QVBoxLayout *verticalLayout;
    QTableView *tableView;

    void setupUi(QDialog *zasoby_biblioteki)
    {
        if (zasoby_biblioteki->objectName().isEmpty())
            zasoby_biblioteki->setObjectName(QStringLiteral("zasoby_biblioteki"));
        zasoby_biblioteki->resize(760, 456);
        verticalLayout = new QVBoxLayout(zasoby_biblioteki);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tableView = new QTableView(zasoby_biblioteki);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setStyleSheet(QLatin1String("#tableView{\n"
"border-image: url(:/items/background.jpg);\n"
"font: 75 12pt \"Nimbus Sans L\";\n"
"color: rgb(255, 255, 255);}"));

        verticalLayout->addWidget(tableView);


        retranslateUi(zasoby_biblioteki);

        QMetaObject::connectSlotsByName(zasoby_biblioteki);
    } // setupUi

    void retranslateUi(QDialog *zasoby_biblioteki)
    {
        zasoby_biblioteki->setWindowTitle(QApplication::translate("zasoby_biblioteki", "Zasoby biblioteki", nullptr));
    } // retranslateUi

};

namespace Ui {
    class zasoby_biblioteki: public Ui_zasoby_biblioteki {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZASOBY_BIBLIOTEKI_H
