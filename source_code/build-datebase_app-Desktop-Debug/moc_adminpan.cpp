/****************************************************************************
** Meta object code from reading C++ file 'adminpan.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../database_project/datebase_app/adminpan.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'adminpan.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_adminpan_t {
    QByteArrayData data[12];
    char stringdata0[295];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_adminpan_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_adminpan_t qt_meta_stringdata_adminpan = {
    {
QT_MOC_LITERAL(0, 0, 8), // "adminpan"
QT_MOC_LITERAL(1, 9, 24), // "on_dodaj_ksiazke_clicked"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 23), // "on_dodaj_autora_clicked"
QT_MOC_LITERAL(4, 59, 27), // "on_przydziel_autora_clicked"
QT_MOC_LITERAL(5, 87, 28), // "on_zasoby_biblioteki_clicked"
QT_MOC_LITERAL(6, 116, 28), // "on_dodaj_wydawnictwo_clicked"
QT_MOC_LITERAL(7, 145, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(8, 167, 30), // "on_uzytkownicy_systemu_clicked"
QT_MOC_LITERAL(9, 198, 25), // "on_znajdz_ksiazke_clicked"
QT_MOC_LITERAL(10, 224, 35), // "on_zarzadzaj_wypozyczeniami_c..."
QT_MOC_LITERAL(11, 260, 34) // "on_zarzadzaj_uzytkownikami_cl..."

    },
    "adminpan\0on_dodaj_ksiazke_clicked\0\0"
    "on_dodaj_autora_clicked\0"
    "on_przydziel_autora_clicked\0"
    "on_zasoby_biblioteki_clicked\0"
    "on_dodaj_wydawnictwo_clicked\0"
    "on_pushButton_clicked\0"
    "on_uzytkownicy_systemu_clicked\0"
    "on_znajdz_ksiazke_clicked\0"
    "on_zarzadzaj_wypozyczeniami_clicked\0"
    "on_zarzadzaj_uzytkownikami_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_adminpan[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x08 /* Private */,
       3,    0,   65,    2, 0x08 /* Private */,
       4,    0,   66,    2, 0x08 /* Private */,
       5,    0,   67,    2, 0x08 /* Private */,
       6,    0,   68,    2, 0x08 /* Private */,
       7,    0,   69,    2, 0x08 /* Private */,
       8,    0,   70,    2, 0x08 /* Private */,
       9,    0,   71,    2, 0x08 /* Private */,
      10,    0,   72,    2, 0x08 /* Private */,
      11,    0,   73,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void adminpan::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        adminpan *_t = static_cast<adminpan *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_dodaj_ksiazke_clicked(); break;
        case 1: _t->on_dodaj_autora_clicked(); break;
        case 2: _t->on_przydziel_autora_clicked(); break;
        case 3: _t->on_zasoby_biblioteki_clicked(); break;
        case 4: _t->on_dodaj_wydawnictwo_clicked(); break;
        case 5: _t->on_pushButton_clicked(); break;
        case 6: _t->on_uzytkownicy_systemu_clicked(); break;
        case 7: _t->on_znajdz_ksiazke_clicked(); break;
        case 8: _t->on_zarzadzaj_wypozyczeniami_clicked(); break;
        case 9: _t->on_zarzadzaj_uzytkownikami_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject adminpan::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_adminpan.data,
      qt_meta_data_adminpan,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *adminpan::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *adminpan::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_adminpan.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int adminpan::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
