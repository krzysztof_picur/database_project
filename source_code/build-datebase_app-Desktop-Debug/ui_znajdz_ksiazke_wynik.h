/********************************************************************************
** Form generated from reading UI file 'znajdz_ksiazke_wynik.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZNAJDZ_KSIAZKE_WYNIK_H
#define UI_ZNAJDZ_KSIAZKE_WYNIK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_znajdz_ksiazke_wynik
{
public:
    QVBoxLayout *verticalLayout;
    QTableView *tableView;

    void setupUi(QDialog *znajdz_ksiazke_wynik)
    {
        if (znajdz_ksiazke_wynik->objectName().isEmpty())
            znajdz_ksiazke_wynik->setObjectName(QStringLiteral("znajdz_ksiazke_wynik"));
        znajdz_ksiazke_wynik->resize(768, 481);
        verticalLayout = new QVBoxLayout(znajdz_ksiazke_wynik);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tableView = new QTableView(znajdz_ksiazke_wynik);
        tableView->setObjectName(QStringLiteral("tableView"));

        verticalLayout->addWidget(tableView);


        retranslateUi(znajdz_ksiazke_wynik);

        QMetaObject::connectSlotsByName(znajdz_ksiazke_wynik);
    } // setupUi

    void retranslateUi(QDialog *znajdz_ksiazke_wynik)
    {
        znajdz_ksiazke_wynik->setWindowTitle(QApplication::translate("znajdz_ksiazke_wynik", "Dialog", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class znajdz_ksiazke_wynik: public Ui_znajdz_ksiazke_wynik {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZNAJDZ_KSIAZKE_WYNIK_H
