/********************************************************************************
** Form generated from reading UI file 'adminpan.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINPAN_H
#define UI_ADMINPAN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_adminpan
{
public:
    QPushButton *dodaj_ksiazke;
    QPushButton *dodaj_autora;
    QPushButton *przydziel_autora;
    QPushButton *zasoby_biblioteki;
    QLabel *label;
    QLabel *label_2;
    QPushButton *dodaj_wydawnictwo;
    QPushButton *pushButton;
    QPushButton *uzytkownicy_systemu;
    QPushButton *znajdz_ksiazke;
    QLabel *label_3;
    QPushButton *zarzadzaj_wypozyczeniami;
    QPushButton *zarzadzaj_uzytkownikami;

    void setupUi(QDialog *adminpan)
    {
        if (adminpan->objectName().isEmpty())
            adminpan->setObjectName(QStringLiteral("adminpan"));
        adminpan->resize(682, 473);
        adminpan->setStyleSheet(QLatin1String("#adminpan{\n"
"border-image: url(:/items/login_background.jpg);}"));
        dodaj_ksiazke = new QPushButton(adminpan);
        dodaj_ksiazke->setObjectName(QStringLiteral("dodaj_ksiazke"));
        dodaj_ksiazke->setGeometry(QRect(20, 70, 51, 51));
        dodaj_ksiazke->setStyleSheet(QLatin1String("#dodaj_ksiazke{\n"
"border-image: url(:/items/addbook.png);\n"
"	\n"
"border-radius: 15px;}\n"
"#dodaj_ksiazke:pressed{\n"
"background-color: rgb(255, 52, 12);\n"
"}"));
        dodaj_autora = new QPushButton(adminpan);
        dodaj_autora->setObjectName(QStringLiteral("dodaj_autora"));
        dodaj_autora->setGeometry(QRect(90, 70, 61, 51));
        dodaj_autora->setStyleSheet(QLatin1String("#dodaj_autora{\n"
"border-image: url(:/items/addauthor.png);\n"
"border-radius: 15px;}\n"
"#dodaj_autora:pressed{\n"
"background-color: rgb(255, 52, 12);\n"
"}"));
        przydziel_autora = new QPushButton(adminpan);
        przydziel_autora->setObjectName(QStringLiteral("przydziel_autora"));
        przydziel_autora->setGeometry(QRect(170, 70, 51, 51));
        przydziel_autora->setMaximumSize(QSize(81, 91));
        przydziel_autora->setStyleSheet(QLatin1String("#przydziel_autora{\n"
"border-image: url(:/items/przypisz_ksiazke.png);\n"
"	\n"
"border-radius: 15px;}\n"
"#przydziel_autora:pressed{\n"
"background-color: rgb(255, 52, 12);\n"
"}"));
        zasoby_biblioteki = new QPushButton(adminpan);
        zasoby_biblioteki->setObjectName(QStringLiteral("zasoby_biblioteki"));
        zasoby_biblioteki->setGeometry(QRect(20, 210, 51, 61));
        zasoby_biblioteki->setStyleSheet(QLatin1String("#zasoby_biblioteki{\n"
"	\n"
"	border-image: url(:/items/book_resources.png);\n"
"border-radius: 15px;}\n"
"#zasoby_biblioteki:pressed{\n"
"background-color: rgb(0, 255, 0);\n"
"}"));
        label = new QLabel(adminpan);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 181, 51));
        label->setStyleSheet(QLatin1String("font: 24pt \"Sans Serif\";\n"
"color: rgb(255, 0, 0);"));
        label_2 = new QLabel(adminpan);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 150, 251, 51));
        label_2->setStyleSheet(QLatin1String("font: 24pt \"Sans Serif\";\n"
"color: rgb(0, 255, 0);\n"
""));
        dodaj_wydawnictwo = new QPushButton(adminpan);
        dodaj_wydawnictwo->setObjectName(QStringLiteral("dodaj_wydawnictwo"));
        dodaj_wydawnictwo->setGeometry(QRect(240, 70, 51, 51));
        dodaj_wydawnictwo->setStyleSheet(QLatin1String("#dodaj_wydawnictwo{\n"
"border-image: url(:/items/dodaj_wydawnictwo.png);\n"
"	\n"
"border-radius: 15px;}\n"
"#dodaj_wydawnictwo:pressed{\n"
"background-color: rgb(255, 52, 12);\n"
"}"));
        pushButton = new QPushButton(adminpan);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(580, 430, 81, 22));
        pushButton->setStyleSheet(QStringLiteral("background-color: rgb(170, 0, 255);"));
        uzytkownicy_systemu = new QPushButton(adminpan);
        uzytkownicy_systemu->setObjectName(QStringLiteral("uzytkownicy_systemu"));
        uzytkownicy_systemu->setGeometry(QRect(90, 210, 51, 61));
        uzytkownicy_systemu->setStyleSheet(QLatin1String("#uzytkownicy_systemu{\n"
"	\n"
"	border-image: url(:/items/wyswietl_userow.png);\n"
"border-radius: 15px;}\n"
"#uzytkownicy_systemu:pressed{\n"
"background-color: rgb(0, 255, 0);\n"
"}"));
        znajdz_ksiazke = new QPushButton(adminpan);
        znajdz_ksiazke->setObjectName(QStringLiteral("znajdz_ksiazke"));
        znajdz_ksiazke->setGeometry(QRect(170, 220, 41, 51));
        znajdz_ksiazke->setStyleSheet(QLatin1String("#znajdz_ksiazke{\n"
"	\n"
"	border-image: url(:/items/search.png);\n"
"border-radius: 15px;}\n"
"#znajdz_ksiazke:pressed{\n"
"background-color: rgb(0, 255, 0);\n"
"}"));
        label_3 = new QLabel(adminpan);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 290, 361, 51));
        label_3->setStyleSheet(QLatin1String("font: 24pt \"Sans Serif\";\n"
"color: rgb(0, 85, 255);\n"
""));
        zarzadzaj_wypozyczeniami = new QPushButton(adminpan);
        zarzadzaj_wypozyczeniami->setObjectName(QStringLiteral("zarzadzaj_wypozyczeniami"));
        zarzadzaj_wypozyczeniami->setGeometry(QRect(20, 350, 51, 61));
        zarzadzaj_wypozyczeniami->setStyleSheet(QLatin1String("#zarzadzaj_wypozyczeniami{\n"
"	\n"
"	border-image: url(:/items/zarzadzaj_wypozyczeniami.png);\n"
"border-radius: 15px;}\n"
"#zarzadzaj_wypozyczeniami:pressed{\n"
"background-color: rgb(0, 85, 255);\n"
"}"));
        zarzadzaj_uzytkownikami = new QPushButton(adminpan);
        zarzadzaj_uzytkownikami->setObjectName(QStringLiteral("zarzadzaj_uzytkownikami"));
        zarzadzaj_uzytkownikami->setGeometry(QRect(90, 350, 51, 61));
        zarzadzaj_uzytkownikami->setStyleSheet(QLatin1String("#zarzadzaj_uzytkownikami{\n"
"border-image: url(:/items/boss.png);\n"
"border-radius: 15px;}\n"
"#zarzadzaj_uzytkownikami:pressed{\n"
"background-color: rgb(0, 85, 255);\n"
"}"));

        retranslateUi(adminpan);

        QMetaObject::connectSlotsByName(adminpan);
    } // setupUi

    void retranslateUi(QDialog *adminpan)
    {
        adminpan->setWindowTitle(QApplication::translate("adminpan", "Panel administratora", nullptr));
#ifndef QT_NO_TOOLTIP
        dodaj_ksiazke->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Dodaj ksi\304\205\305\274k\304\231</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        dodaj_ksiazke->setText(QString());
#ifndef QT_NO_TOOLTIP
        dodaj_autora->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Dodaj autora</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        dodaj_autora->setText(QString());
#ifndef QT_NO_TOOLTIP
        przydziel_autora->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Przydziel Autora Ksi\304\205\305\274ce</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_WHATSTHIS
        przydziel_autora->setWhatsThis(QApplication::translate("adminpan", "<html><head/><body><p><br/></p></body></html>", nullptr));
#endif // QT_NO_WHATSTHIS
        przydziel_autora->setText(QString());
#ifndef QT_NO_TOOLTIP
        zasoby_biblioteki->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Sprawd\305\272 zasoby biblioteki</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        zasoby_biblioteki->setText(QString());
        label->setText(QApplication::translate("adminpan", "Dodaj :", nullptr));
        label_2->setText(QApplication::translate("adminpan", "Wy\305\233wietl :", nullptr));
#ifndef QT_NO_TOOLTIP
        dodaj_wydawnictwo->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Dodaj wydawnictwo</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        dodaj_wydawnictwo->setText(QString());
        pushButton->setText(QApplication::translate("adminpan", "Wyloguj", nullptr));
#ifndef QT_NO_TOOLTIP
        uzytkownicy_systemu->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Wy\305\233wietl u\305\274ytkownik\303\263w sysemu</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        uzytkownicy_systemu->setText(QString());
#ifndef QT_NO_TOOLTIP
        znajdz_ksiazke->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Znajd\305\272 ksi\304\205\305\274k\304\231</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        znajdz_ksiazke->setText(QString());
        label_3->setText(QApplication::translate("adminpan", "Zarzadzaj/Aktualizuj :", nullptr));
#ifndef QT_NO_TOOLTIP
        zarzadzaj_wypozyczeniami->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Zarz\304\205dzaj wypo\305\274yczeniami</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        zarzadzaj_wypozyczeniami->setText(QString());
#ifndef QT_NO_TOOLTIP
        zarzadzaj_uzytkownikami->setToolTip(QApplication::translate("adminpan", "<html><head/><body><p><span style=\" font-weight:600;\">Zarz\304\205dzaj u\305\274ytkownikami</span></p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        zarzadzaj_uzytkownikami->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class adminpan: public Ui_adminpan {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINPAN_H
