/********************************************************************************
** Form generated from reading UI file 'znajdz_ksiazke.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZNAJDZ_KSIAZKE_H
#define UI_ZNAJDZ_KSIAZKE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_znajdz_ksiazke
{
public:
    QVBoxLayout *verticalLayout_2;
    QTableView *tableView;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *tytul;
    QLabel *label_3;
    QLineEdit *ISBN;
    QLabel *label_2;
    QLineEdit *gatunek;
    QPushButton *Szukaj_ksiazki;

    void setupUi(QDialog *znajdz_ksiazke)
    {
        if (znajdz_ksiazke->objectName().isEmpty())
            znajdz_ksiazke->setObjectName(QStringLiteral("znajdz_ksiazke"));
        znajdz_ksiazke->resize(723, 496);
        verticalLayout_2 = new QVBoxLayout(znajdz_ksiazke);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        tableView = new QTableView(znajdz_ksiazke);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setStyleSheet(QLatin1String("\n"
"background-color: rgb(76, 76, 76);\n"
"font: 75 12pt \"Nimbus Sans L\";\n"
"\n"
"\n"
""));

        verticalLayout_2->addWidget(tableView);

        widget = new QWidget(znajdz_ksiazke);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setStyleSheet(QLatin1String("#widget{\n"
"background-color: rgb(0, 170, 127);}"));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        tytul = new QLineEdit(widget);
        tytul->setObjectName(QStringLiteral("tytul"));

        horizontalLayout->addWidget(tytul);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout->addWidget(label_3);

        ISBN = new QLineEdit(widget);
        ISBN->setObjectName(QStringLiteral("ISBN"));

        horizontalLayout->addWidget(ISBN);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        gatunek = new QLineEdit(widget);
        gatunek->setObjectName(QStringLiteral("gatunek"));

        horizontalLayout->addWidget(gatunek);

        Szukaj_ksiazki = new QPushButton(widget);
        Szukaj_ksiazki->setObjectName(QStringLiteral("Szukaj_ksiazki"));
        Szukaj_ksiazki->setStyleSheet(QStringLiteral("background-color: rgb(85, 255, 0);"));

        horizontalLayout->addWidget(Szukaj_ksiazki);


        verticalLayout_2->addWidget(widget);


        retranslateUi(znajdz_ksiazke);

        QMetaObject::connectSlotsByName(znajdz_ksiazke);
    } // setupUi

    void retranslateUi(QDialog *znajdz_ksiazke)
    {
        znajdz_ksiazke->setWindowTitle(QApplication::translate("znajdz_ksiazke", "Znajd\305\272 ksi\304\205\305\274k\304\231", nullptr));
        label->setText(QApplication::translate("znajdz_ksiazke", "Tytu\305\202", nullptr));
        label_3->setText(QApplication::translate("znajdz_ksiazke", "ISBN", nullptr));
        label_2->setText(QApplication::translate("znajdz_ksiazke", "Gatunek", nullptr));
        Szukaj_ksiazki->setText(QApplication::translate("znajdz_ksiazke", "Szukaj", nullptr));
    } // retranslateUi

};

namespace Ui {
    class znajdz_ksiazke: public Ui_znajdz_ksiazke {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZNAJDZ_KSIAZKE_H
