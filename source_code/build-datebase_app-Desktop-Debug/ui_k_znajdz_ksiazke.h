/********************************************************************************
** Form generated from reading UI file 'k_znajdz_ksiazke.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_K_ZNAJDZ_KSIAZKE_H
#define UI_K_ZNAJDZ_KSIAZKE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>

QT_BEGIN_NAMESPACE

class Ui_k_znajdz_ksiazke
{
public:
    QTableView *tableView;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLineEdit *linepodtytul;
    QLabel *label_7;
    QLineEdit *lineautorimie;
    QLabel *label;
    QLabel *label_4;
    QLineEdit *lineoprawa;
    QLineEdit *linegatunek;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *lineautornazwisko;
    QLineEdit *linetytul;
    QLineEdit *lineisbn;
    QPushButton *szukaj;
    QPushButton *szukaj_2;
    QGroupBox *groupBox_2;
    QPushButton *wypozycz;
    QLabel *label_9;
    QPushButton *pushButton;

    void setupUi(QDialog *k_znajdz_ksiazke)
    {
        if (k_znajdz_ksiazke->objectName().isEmpty())
            k_znajdz_ksiazke->setObjectName(QStringLiteral("k_znajdz_ksiazke"));
        k_znajdz_ksiazke->resize(951, 638);
        k_znajdz_ksiazke->setStyleSheet(QLatin1String("#k_znajdz_ksiazke{\n"
"background-image: url(:/items/bacground_client.jpg);\n"
"}"));
        tableView = new QTableView(k_znajdz_ksiazke);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setGeometry(QRect(10, 10, 931, 341));
        groupBox = new QGroupBox(k_znajdz_ksiazke);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(40, 360, 401, 261));
        groupBox->setStyleSheet(QLatin1String("QGroupBox{\n"
"background-color: rgb(233, 185, 110);\n"
"border-radius: 10px;\n"
"}"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 1, 1, 1);

        linepodtytul = new QLineEdit(groupBox);
        linepodtytul->setObjectName(QStringLiteral("linepodtytul"));

        gridLayout->addWidget(linepodtytul, 3, 0, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 6, 0, 1, 1);

        lineautorimie = new QLineEdit(groupBox);
        lineautorimie->setObjectName(QStringLiteral("lineautorimie"));

        gridLayout->addWidget(lineautorimie, 3, 1, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 2, 1, 1, 1);

        lineoprawa = new QLineEdit(groupBox);
        lineoprawa->setObjectName(QStringLiteral("lineoprawa"));

        gridLayout->addWidget(lineoprawa, 7, 0, 1, 1);

        linegatunek = new QLineEdit(groupBox);
        linegatunek->setObjectName(QStringLiteral("linegatunek"));

        gridLayout->addWidget(linegatunek, 5, 0, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 4, 1, 1, 1);

        lineautornazwisko = new QLineEdit(groupBox);
        lineautornazwisko->setObjectName(QStringLiteral("lineautornazwisko"));

        gridLayout->addWidget(lineautornazwisko, 5, 1, 1, 1);

        linetytul = new QLineEdit(groupBox);
        linetytul->setObjectName(QStringLiteral("linetytul"));

        gridLayout->addWidget(linetytul, 1, 0, 1, 1);

        lineisbn = new QLineEdit(groupBox);
        lineisbn->setObjectName(QStringLiteral("lineisbn"));

        gridLayout->addWidget(lineisbn, 1, 1, 1, 1);

        szukaj = new QPushButton(groupBox);
        szukaj->setObjectName(QStringLiteral("szukaj"));

        gridLayout->addWidget(szukaj, 8, 0, 1, 1);

        szukaj_2 = new QPushButton(groupBox);
        szukaj_2->setObjectName(QStringLiteral("szukaj_2"));

        gridLayout->addWidget(szukaj_2, 8, 1, 1, 1);

        groupBox_2 = new QGroupBox(k_znajdz_ksiazke);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(460, 360, 211, 261));
        groupBox_2->setStyleSheet(QLatin1String("QGroupBox{\n"
"background-color: rgb(233, 185, 110);\n"
"border-radius: 10px;\n"
"}"));
        wypozycz = new QPushButton(groupBox_2);
        wypozycz->setObjectName(QStringLiteral("wypozycz"));
        wypozycz->setGeometry(QRect(10, 90, 191, 71));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 10, 191, 71));
        label_9->setScaledContents(false);
        label_9->setWordWrap(true);
        pushButton = new QPushButton(k_znajdz_ksiazke);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(820, 590, 101, 23));

        retranslateUi(k_znajdz_ksiazke);

        QMetaObject::connectSlotsByName(k_znajdz_ksiazke);
    } // setupUi

    void retranslateUi(QDialog *k_znajdz_ksiazke)
    {
        k_znajdz_ksiazke->setWindowTitle(QApplication::translate("k_znajdz_ksiazke", "Wyszukaj Ksi\304\205\305\274k\304\231", nullptr));
        groupBox->setTitle(QString());
        label_2->setText(QApplication::translate("k_znajdz_ksiazke", "ISBN", nullptr));
        label_7->setText(QApplication::translate("k_znajdz_ksiazke", "Oprawa", nullptr));
        label->setText(QApplication::translate("k_znajdz_ksiazke", "Tytu\305\202", nullptr));
        label_4->setText(QApplication::translate("k_znajdz_ksiazke", "Autor Imie", nullptr));
        label_3->setText(QApplication::translate("k_znajdz_ksiazke", "Podtytu\305\202", nullptr));
        label_5->setText(QApplication::translate("k_znajdz_ksiazke", "Gatunek", nullptr));
        label_6->setText(QApplication::translate("k_znajdz_ksiazke", "Autor Nazwisko", nullptr));
        szukaj->setText(QApplication::translate("k_znajdz_ksiazke", "Szukaj", nullptr));
        szukaj_2->setText(QApplication::translate("k_znajdz_ksiazke", "Wy\305\233wietl wszystkie", nullptr));
#ifndef QT_NO_ACCESSIBILITY
        groupBox_2->setAccessibleDescription(QString());
#endif // QT_NO_ACCESSIBILITY
        groupBox_2->setTitle(QString());
        wypozycz->setText(QApplication::translate("k_znajdz_ksiazke", "Wypo\305\274ycz", nullptr));
        label_9->setText(QApplication::translate("k_znajdz_ksiazke", "Wszystkie Materia\305\202y Wypo\305\274yczamy na okres miesi\304\205ca zapoznaj sie z regulaminem", nullptr));
        pushButton->setText(QApplication::translate("k_znajdz_ksiazke", "Panel Klienta", nullptr));
    } // retranslateUi

};

namespace Ui {
    class k_znajdz_ksiazke: public Ui_k_znajdz_ksiazke {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_K_ZNAJDZ_KSIAZKE_H
