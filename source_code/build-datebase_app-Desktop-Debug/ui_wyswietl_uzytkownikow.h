/********************************************************************************
** Form generated from reading UI file 'wyswietl_uzytkownikow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WYSWIETL_UZYTKOWNIKOW_H
#define UI_WYSWIETL_UZYTKOWNIKOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_wyswietl_uzytkownikow
{
public:
    QVBoxLayout *verticalLayout;
    QTableView *tableView;

    void setupUi(QDialog *wyswietl_uzytkownikow)
    {
        if (wyswietl_uzytkownikow->objectName().isEmpty())
            wyswietl_uzytkownikow->setObjectName(QStringLiteral("wyswietl_uzytkownikow"));
        wyswietl_uzytkownikow->resize(875, 512);
        verticalLayout = new QVBoxLayout(wyswietl_uzytkownikow);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tableView = new QTableView(wyswietl_uzytkownikow);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setStyleSheet(QLatin1String("#tableView{\n"
"border-image: url(:/items/wyswietl_uzytkownikow.jpeg);\n"
"	color: rgb(255, 255, 255);\n"
"\n"
"	font: 75 11pt \"Liberation Sans\";\n"
"background-color: rgb(255, 255, 255);\n"
"}"));

        verticalLayout->addWidget(tableView);


        retranslateUi(wyswietl_uzytkownikow);

        QMetaObject::connectSlotsByName(wyswietl_uzytkownikow);
    } // setupUi

    void retranslateUi(QDialog *wyswietl_uzytkownikow)
    {
        wyswietl_uzytkownikow->setWindowTitle(QApplication::translate("wyswietl_uzytkownikow", "Wy\305\233wietl u\305\274ytkownik\303\263w", nullptr));
    } // retranslateUi

};

namespace Ui {
    class wyswietl_uzytkownikow: public Ui_wyswietl_uzytkownikow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WYSWIETL_UZYTKOWNIKOW_H
